import bpy
import sys
import os
import numpy as np
import bmesh
from bpy.types import Panel, Operator, PropertyGroup
from bpy.props import EnumProperty, PointerProperty, StringProperty
from mathutils import Color, Vector


class MyProperties(PropertyGroup):
    
    my_enum : EnumProperty(
        name= "Enumerator / Dropdown",
        description= "Chooses the face of polygons for boundary conditions.",
        items= [('OP1', "p = 0", ""),
                ('OP2', "p = 1", ""),
                ('OP3', "q = 0", ""),
                ('OP4', "q = 1", "")
        ]
    )

class ADDONAME_PT_main_panel(Panel):
    bl_label = "Blender Cat"
    bl_idname = "ADDONAME_PT_main_panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "BlenderCAT"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        mytool = scene.my_tool
        
        layout.operator("addonname.addcube_operator")
        layout.prop(mytool, "my_enum", expand = True)

class ADDONAME_OT_addcube(Operator):
    bl_label = "Add an exterior cube"
    bl_idname = "addonname.addcube_operator"

    def execute(self, context):
        bpy.ops.mesh.primitive_cube_add(enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
#        bpy.ops.object.subdivision_set(level=3, relative=False)
#        bpy.context.object.modifiers["Subdivision"].subdivision_type = 'SIMPLE'
#        bpy.ops.object.modifier_apply(modifier="Subdivision")
#        bpy.ops.object.editmode_toggle()
#        bpy.ops.mesh.normals_make_consistent(inside=True)
#        bpy.ops.object.editmode_toggle()

        return {'FINISHED'}


classes = [MyProperties, ADDONAME_OT_addcube]
 
def register():
    for cls in classes:
        bpy.utils.register_class(cls)
        
    bpy.types.Scene.my_tool = PointerProperty(type= MyProperties)
 
def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.my_tool
 
if __name__ == "__main__":
    register()